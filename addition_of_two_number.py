def print_addition(first_number, second_number):
    """
    :type first_number: int
    :type second_number: int

    """
    return first_number + second_number


def print_subtraction(first_number, second_number):
    """
    :type first_number: int
    :type second_number: int
    """
    return first_number - second_number


if __name__ == "__main__":
    print("Addition Result is :", print_addition(10, 20))
    print("Subtraction Result is :", print_subtraction(10, 20))

