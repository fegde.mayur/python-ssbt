def swap_numbers(first_number, second_number):
    """
    :type first_number: int
    :type second_number: int

    """
    temp = first_number
    first_number = second_number
    second_number = temp

    return first_number, second_number


if __name__ == "__main__":
    first_number, second_number = 10, 20
    print("===== Before ======")
    print("First number is :", first_number)
    print("Second number is :", second_number)
    first_number, second_number = swap_numbers(10, 20)
    print("===== After ======")
    print("First number is :", first_number)
    print("Second number is :", second_number)
