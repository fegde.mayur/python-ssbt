even_number_list, odd_number_list = list(), list()

for number in [0,1,2,3,4]:
    if number % 2 == 0:
        even_number_list.append(number)
    else:
        odd_number_list.append(number)

print("Even Number list is :", even_number_list)
print("Odd Number list is :", odd_number_list)
